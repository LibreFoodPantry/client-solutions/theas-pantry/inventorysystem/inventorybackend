---
openapi: 3.0.3
servers:
  - url: 'http://localhost'
info:
  title: InventoryBackend
  version: 0.1.0
  description: Weight-Based Inventory information for Thea's Pantry
  contact:
    name: Thea's Pantry
    email: info@librefoodpantry.org
    url: librefoodpantry.org
tags:
  - name: inventory
    description: Weight Based Inventory to Thea's Pantry
  - name: version
    description: API version
paths:
  /inventory:
    get:
      description: Gets the total weight in the inventory.
      tags:
        - inventory
      operationId: getInventory
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Weight'
        '400':
          $ref: '#/components/responses/Unauthorized'
    patch:
      description: Updates weight in inventory.
      tags:
        - inventory
      operationId: updateInventory
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateRequest'
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Weight'
          description: Updated.
        '400':
          $ref: '#/components/responses/Unauthorized'
  /version:
    description: Get current API version number
    get:
      description: Gets current operating version of the API
      tags:
        - inventory
      operationId: getAPIVersion
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/APIVersion'
components:
  responses:
    Unauthorized:
      description: No valid token was found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EovError'
  schemas:
    UpdateRequest:
      description: Identification of individual taking from the inventory.
      type: object
      properties:
        Weight:
          $ref: '#/components/schemas/Weight'
        Id:
          $ref: '#/components/schemas/Id'
    EovError:
      description: Should be either changed or removed entirely
      type: object
      example:
        message: "'.response should have required property ''name'',\
                   .response should have required property ''id'''"
        errors:
          - path: .response.name
            message: should have required property 'name'
            errorCode: required.openapi.validation
          - path: .response.id
            message: should have required property 'id'
            errorCode: required.openapi.validation
      properties:
        message:
          type: string
        errors:
          type: array
          items:
            type: object
            properties:
              path:
                type: string
              message:
                type: string
              errorCode:
                type: string
    Id:
      description: Identification of individual taking from the inventory.
      type: string
    Weight:
      description: Weight of the items in the inventory.
      type: number
      nullable: false
    APIVersion:
      example: 1.2.3
      description: Current operating version of the API
      # yamllint disable-line rule:line-length
      pattern: ^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$
      type: string
