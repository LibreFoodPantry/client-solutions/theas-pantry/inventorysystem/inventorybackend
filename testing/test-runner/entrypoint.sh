#!/usr/bin/env sh

echo "servers:" >> /tmp/openapi.yaml
echo "  - url: '${SUT_BASE_URL}'" >> /tmp/openapi.yaml
node ./node_modules/mocha/bin/mocha -C
