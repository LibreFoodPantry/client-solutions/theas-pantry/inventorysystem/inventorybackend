"use strict";
const Inventory = require("../data/inventory.js");

module.exports = {
  method: 'patch',
  path: '/inventory',
  async handler(request, response) {
    const itemWeight = request.body.weight;
    const inventory = await Inventory.updateInventoryWeight(itemWeight);
    try {
      response.status(200).json(inventory);
    } catch (e) {
      logger.error('InventoryAccessObject.updateInventory', e)
      throw {
          code: 500,
          error: 'Internal Server Error',
          caused_by: e,
      }
    }
  }
};
