"use strict";
const Inventory = require("../data/inventory.js");

module.exports = {
    method: 'get',
    path: '/inventory',
    async handler(request, response) {
        let totalWeight = await Inventory.getInventory();
        try {
            response.status(200).json(totalWeight);
        } catch (e) {
            logger.error('InventoryAccessObject.getTotal', e)
            throw {
                code: 500,
                error: 'Internal Server Error',
                caused_by: e,
            }
        }
    }
};
